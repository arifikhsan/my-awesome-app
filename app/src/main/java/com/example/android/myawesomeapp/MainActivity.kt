package com.example.android.myawesomeapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.example.android.myawesomeapp.network.NetworkManager
import com.example.android.myawesomeapp.network.NetworkService
import com.example.android.myawesomeapp.section.news.NewsAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        NetworkManager.createService(NetworkService::class.java)
                .getListNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    var adapter = NewsAdapter(this@MainActivity, it.body()!!.result)
                    recyclerView1.adapter = adapter
                    recyclerView1.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                }, {
                    Toast.makeText(this, R.string.message_problem, Toast.LENGTH_LONG).show()
                })

    }

}
