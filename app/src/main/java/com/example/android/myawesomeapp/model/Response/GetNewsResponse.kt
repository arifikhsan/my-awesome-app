package com.example.android.myawesomeapp.model.Response

import com.example.android.myawesomeapp.model.News
import com.google.gson.annotations.SerializedName

/**
 * Created by isan on 14/09/18.
 */

data class GetNewsResponse(@SerializedName("status") val status: String,
                           @SerializedName("result") val result: MutableList<News>)
