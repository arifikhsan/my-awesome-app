package com.example.android.myawesomeapp.section.news

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.example.android.myawesomeapp.R


/**
 * Created by Arif Ikhsanudin on Saturday, 15 September 2018.
 */

class NewsDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)

        val tv1 = findViewById<View>(R.id.content) as TextView
        if (intent.extras != null) {
            val test = intent.extras!!.getString("test")
            tv1.text = test
        }

    }

}
