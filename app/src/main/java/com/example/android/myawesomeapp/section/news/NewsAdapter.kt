package com.example.android.myawesomeapp.section.news

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.example.android.myawesomeapp.R
import com.example.android.myawesomeapp.model.News


/**
 * Created by Arif Ikhsanudin on Saturday, 15 September 2018.
 */


class NewsAdapter(val context: Context,val news: MutableList<News>): RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtTitle?.text = news[position].title
        holder.txtContent?.text = getFirstNStrings(news[position].content, 10)

        holder.itemView.setOnClickListener {
//            Toast.makeText(context, news[position].content, Toast.LENGTH_LONG).show()
            val intent = Intent(context, NewsDetailActivity::class.java)
            val b = Bundle()
            b.putString("test", news[position].content)
            intent.putExtras(b)

            context.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.activity_news, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return news.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val txtTitle = itemView.findViewById<TextView>(R.id.txtTitle)
        val txtContent = itemView.findViewById<TextView>(R.id.txtContent)
    }

    fun getFirstNStrings(str: String, words: Int): String {
        val sArr = str.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var firstStrs = ""
        for (i in 0 until words)
            firstStrs += sArr[i] + " "
        return firstStrs.trim { it <= ' ' }
    }

}
