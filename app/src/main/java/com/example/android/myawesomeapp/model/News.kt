package com.example.android.myawesomeapp.model

import com.google.gson.annotations.SerializedName

/**
 * Created by isan on 14/09/18.
 */

data class News(@SerializedName("id") var id: Int,
                @SerializedName("title") var title: String,
                @SerializedName("content") var content: String)


