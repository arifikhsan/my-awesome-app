package com.example.android.myawesomeapp.network

import com.example.android.myawesomeapp.model.Response.GetNewsResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

/**
 * Created by isan on 14/09/18.
 */

interface NetworkService {

    @GET("api/v1/news")
    fun getListNews(): Observable<Response<GetNewsResponse>>

}
